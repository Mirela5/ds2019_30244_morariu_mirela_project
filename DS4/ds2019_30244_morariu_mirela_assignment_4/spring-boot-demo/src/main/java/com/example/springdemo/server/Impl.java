package com.example.springdemo.server;


import com.example.springdemo.entities.MedicationPlan;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Impl implements Server{
    ArrayList<String> list_start=new ArrayList<String>();
    ArrayList<String> list_end=new ArrayList<String>();
    ArrayList<String> id=new ArrayList<String>();

    String activity;

    public Impl(String activity)
    {
        try {
            getActivityData(activity);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public Impl(){}

    @Override
    public String[] getActivityData(String activity_name) throws SQLException, ParseException {
        String url = "jdbc:mysql://localhost:3306/tema3";
        String userName = "root";
        String password = "";
        //int i=0;
        String[] array = new String[2];
        String start_time=null;
        String end_time=null;
        String id_f = null;


        Connection conn = null;
        Statement stmt = null;


        //Register JDBC driver
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        //Open a connection
        System.out.println("Connecting to a selected database...");
        try {
            conn = DriverManager.getConnection(url, userName, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        MedicationPlan medicationPlan = new MedicationPlan();
        try {
            stmt = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String sql = "SELECT id, start_time, end_time FROM activity WHERE activity_name= RTRIM('" + activity_name + "')";
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //Extract data from result set
        while (true) {
            try {
                if (!rs.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();
            }

            //taken = rs.getInt("taken");
            start_time=rs.getString("start_time");
            end_time=rs.getString("end_time");
            id_f = rs.getString("id");


            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date start_date = (Date) df.parse(start_time);
            Date end_date = (Date)df.parse(end_time);
            String newStartDateString = df.format(start_date);
            String newEndDateString = df.format(end_date);


            list_start.add(newStartDateString);
            list_end.add(newEndDateString);
            id.add(id_f);
            System.out.println("ID : " + id_f + " | Start date : " + newStartDateString + " | End date: " + newEndDateString + "\n" + "\n" );
            // list_end.add(end_time);




            // array[i]= Integer.toString(taken);
            // ++i;



        }

       // System.out.println(list_start);
        // System.out.println(list_end);
        rs.close();
        return array;
    }

    public  ArrayList<String> getList_start() {


        return list_start;
    }

    public ArrayList<String> getId() {
        return id;
    }

    public void setList_start(ArrayList<String> list_start) {
        this.list_start = list_start;
    }

    public ArrayList<String> getList_end() {

        return list_end;
    }

    public void setList_end(ArrayList<String> list_end) {
        this.list_end = list_end;
    }
}

