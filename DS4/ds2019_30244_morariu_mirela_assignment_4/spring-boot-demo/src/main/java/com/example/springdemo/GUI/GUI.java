package com.example.springdemo.GUI;
import com.example.springdemo.server.Impl;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;



import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.image.ImageObserver;
import java.sql.SQLException;
import java.text.AttributedCharacterIterator;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.ArrayList;
import java.util.Date;

public class GUI extends Canvas implements ActionListener, ChangeListener {
    static final long serialVersionUID = -1255148347247167282L;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
    private JTextArea logger;
    private JTextArea rec;
    private JFrame wrapper;
    private ChartPanel chartPanel;
    JLabel select = new JLabel("Select Data : ");
    JLabel recomendation = new JLabel("Send recomendation : ");
    JTextField t3;
    JButton b = new JButton("Send");
    JButton b12 = new JButton("Display");
    JTextField t1 ;
    JTextField t2 ;
    DefaultPieDataset dataset;
    JLabel label;

    public GUI() {

        wrapper = new JFrame("Tema4");
        wrapper.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        wrapper.setSize(1980, 1080);


        select.setBounds(50, 50, 200, 30);

        b.setBounds(50, 200, 130, 30);

        t1 = new JTextField("Enter id");
        t1.setBounds(50, 100, 200, 30);
        t1.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                JTextField source = (JTextField)e.getComponent();
                source.setText("");
                source.removeFocusListener(this);
            }
        });

        t2 = new JTextField("Enter activity");
        t2.setBounds(50, 150, 200, 30);
        t2.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                JTextField source = (JTextField)e.getComponent();
                source.setText("");
                source.removeFocusListener(this);
            }
        });

        recomendation.setBounds(500, 50, 200, 30);
        t3 = new JTextField("Enter recomandation");
        t3.setBounds(500, 100, 200, 30);
        t3.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
                JTextField source = (JTextField)e.getComponent();
                source.setText("");
                source.removeFocusListener(this);
            }
        });

        rec = new JTextArea();
        JScrollPane loggerScroller2 = new JScrollPane(rec);
        loggerScroller2.setBounds(800, 50, 500, 230);
        wrapper.add(loggerScroller2);

        b12.setBounds(500, 150, 130, 30);
        b12.addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                String text = t3.getText();
                println2("\nYour recomandation : ");
                println2(" - " + text);
                println2("\n All recomandation : ");
                println2(" - Recomandation 1 \n - Recomandation 2 \n - Recomandation 3 \n - Recomandation 4");
                println2(" - " + text);

            }
        });



        wrapper.add(b12);
        wrapper.add(recomendation);
        wrapper.add(t3);

        label = new JLabel(" ");
        label.setBounds(200,0, 100,100);

        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                while (true) {
                    Date date = getDate();
                    String dateString = simpleDateFormat.format(date);
                    label.setText(dateString);


                    try {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        Thread t = new Thread(runnable);
        t.start();

        wrapper.add(label);
        wrapper.add(t2);
        b.addActionListener(this);

        logger = new JTextArea();
        JScrollPane loggerScroller = new JScrollPane(logger);
        loggerScroller.setBounds(50, 300, 700, 350);


        wrapper.add(loggerScroller);
        wrapper.add(t1);
        wrapper.add(select);
        wrapper.add(b);
        wrapper.setResizable(true);
        wrapper.getContentPane().setLayout(null);
        wrapper.setLocationRelativeTo(null);
        wrapper.setVisible(true);


    }

    private void println2(String s) {
        rec.append(s + "\n");
        rec.setCaretPosition(logger.getText().length());
    }


    public static void main (String[] args)
    {
        new GUI();

    }

    public void prinln(String s) {
        logger.append(s + "\n");
        logger.setCaretPosition(logger.getText().length());
    }

    public int i = 0;

    @Override
    // button listener
    public void actionPerformed(ActionEvent arg0)
    {

        dataset = new DefaultPieDataset();

        String textFieldValue = t2.getText();
        String id = t1.getText();

        dataset.setValue(textFieldValue, 10.0);
        logger.setText("");

        prinln(  " DATA : 13-12-2019 \n");
        i++;

        if( id != null )
          prinln(  " ID patient : " + id +  "  Activity : " + textFieldValue);
        else {
            if(textFieldValue != null   )
                prinln(" Activity : " + textFieldValue);
        }
        Impl impl = new Impl(textFieldValue);

        int size = impl.getList_start().size();

        if(size!= 0) {
            for (int i = 0; i < size; i++) {
                prinln(" - ID : " + impl.getId().get(i) + "  |  Start time : " + impl.getList_start().get(i) + "  |  End time : " + impl.getList_end().get(i));
            }
        }
        else
        {
            prinln(" Activity not found ! ");
        }

        // dataset.setValue("Showering", avg2);
        //dataset.setValue("Sleeping", 12);
        dataset.setValue("Dinner", 3);
        dataset.setValue("Lunch", 2);
        dataset.setValue("Others",1);

        JFreeChart chart = createChart(dataset);
        chartPanel = new ChartPanel(chart);
        chartPanel.setBounds(800, 300, 500,350);
        chartPanel.setBackground(Color.white);

        wrapper.add(chartPanel);
        wrapper.repaint();
    }



    private DefaultPieDataset createDataset(String activity, double avg) {

        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue(activity, avg);
        // dataset.setValue("Showering", avg2);
        //dataset.setValue("Sleeping", 12);
        dataset.setValue("Breakfast", 3);
        dataset.setValue("Lunch", 1);
        //dataset.setValue("Others", 2);

        return dataset;
    }

    private JFreeChart createChart(DefaultPieDataset dataset) {

        JFreeChart pieChart = ChartFactory.createPieChart(
                "Patient history daily activities",
                dataset,
                false, true, false);

        return pieChart;
    }


    public static java.util.Date getDate() {
        java.util.Date date = new java.util.Date();
        return date;
    }


    @Override
    public void stateChanged(ChangeEvent e) {

    }
}
