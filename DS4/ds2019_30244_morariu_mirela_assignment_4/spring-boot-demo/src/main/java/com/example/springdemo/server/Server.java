package com.example.springdemo.server;

import java.sql.SQLException;
import java.text.ParseException;

public interface Server
{

    String[] getActivityData(String activity_name) throws SQLException, ParseException;
}

