package com.example.springdemo.rmi;

import GUI.mainGUI;
import com.example.springdemo.entities.MedicationPlan;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Client {

    public static mainGUI mainGUI;
    public static Integer number = 0;
    public static String start;
    public static String end;

    public static ArrayList<Integer> delete ;

    public Client(ArrayList delete) throws Exception {
        this.delete = delete;
        logger(listCreater(), delete);
    }

    public static void main(String args[]) throws Exception
    {

        try {
            // Getting the registry
            Registry registry = LocateRegistry.getRegistry(null);

            // Looking up the registry for the remote object
            Hello stub = (Hello) registry.lookup("Hello");

            // Calling the remote method using the obtained object
            List<MedicationPlan> list = (List) stub.getMedicationPlan();

            // System.out.println(list);
            all(list, delete);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }

    }

    public List<MedicationPlan> listCreater ()
    {
        try {
            // Getting the registry
            Registry registry = LocateRegistry.getRegistry(null);

            // Looking up the registry for the remote object
            Hello stub = (Hello) registry.lookup("Hello");

            // Calling the remote method using the obtained object
            List<MedicationPlan> list = (List) stub.getMedicationPlan();

            // System.out.println(list);

            return list;
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }


        return null;
    }

    public static void all(List <MedicationPlan> list, ArrayList delete) throws Exception {

        for (MedicationPlan med : list) {
            number++;
        }
        mainGUI = new mainGUI(number);
        delete = new ArrayList<Integer>(number);
        mainGUI.prinln("\n");
        mainGUI.prinln("All medications plans\n");
        for (MedicationPlan med : list) {

            System.out.println("Id: " + med.getId());
            mainGUI.prinln(" id: " + med.getId());
            System.out.println("name: " + med.getName());
            mainGUI.prinln(" name: " + med.getName());
            System.out.println("start date: " + med.getStartDate());
            start = med.getStartDate();
            mainGUI.prinln(" start date: " + med.getStartDate());
            System.out.println("end date: " + med.getEndDate());
            end = med.getEndDate();
            mainGUI.prinln(" end date: " + med.getEndDate());
            mainGUI.prinln(" ---------------------------");
        }

    }


    private static void logger(List<MedicationPlan> list,ArrayList delete) {


        mainGUI.prinln2("\n");
        mainGUI.prinln2("Remaining medications plans\n");

        int found = 0;
        System.out.println(delete);
         for (MedicationPlan med : list) {


             for( int i =0 ; i<delete.size(); i++)
             {
                 if ( delete.get(i).equals((Integer)med.getId())) {
                     found = 1;
                 }
             }

             if(found == 0)
             {
                     System.out.println("Id: " + med.getId());
                     mainGUI.prinln2(" id: " + med.getId());
                     System.out.println("name: " + med.getName());
                     mainGUI.prinln2(" name: " + med.getName());
                     System.out.println("start date: " + med.getStartDate());
                     start = med.getStartDate();
                     mainGUI.prinln2(" start date: " + med.getStartDate());
                     System.out.println("end date: " + med.getEndDate());
                     end = med.getEndDate();
                     mainGUI.prinln2(" end date: " + med.getEndDate());
                     mainGUI.prinln2(" ---------------------------");
             }

            found = 0;
        }
    }


    public static String getStart() {
        return start;
    }

    public static String getEnd() {
        return end;
    }
}