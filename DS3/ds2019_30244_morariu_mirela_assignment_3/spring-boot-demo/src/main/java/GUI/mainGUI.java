package GUI;

import com.example.springdemo.rmi.Client;

import java.awt.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.event.*;
import java.util.ArrayList;
import java.util.Date;

public class mainGUI extends JPanel implements ActionListener, ChangeListener {
    static final long serialVersionUID = -1255148347247167282L;

    private JTextArea logger;
    private JTextArea messageDisplayer;
    private JButton refresh;
    private JFrame wrapper;
    private JSlider refreshRate = null;
    private Simulation sim;
    public ArrayList<Integer> delete ;
    private JLabel label;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
    private static Integer number = 0;
    private JButton taken;
    private Integer j=0;

    public mainGUI(Integer number) {
        wrapper = new JFrame("Tema 3");


        wrapper.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        wrapper.setSize(440, 720);


        logger = new JTextArea();
        JScrollPane loggerScroller = new JScrollPane(logger);
        loggerScroller.setBounds(10, 65, 400, 350);

        messageDisplayer = new JTextArea();
        JScrollPane loggerScroller2 = new JScrollPane(messageDisplayer);
        loggerScroller2.setBackground(Color.CYAN);
        loggerScroller2.setBounds(10, 420, 400, 250);


        delete = new ArrayList<Integer>(number);
        Integer rowY = 70;
        for ( int i=1; i<= number ; i++)
        {
            JButton taken = new JButton("Taken");
            taken.setBounds(250, rowY, 100, 50);
            taken.setBackground(Color.orange);
            int finalI = i;
            taken.addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(java.awt.event.ActionEvent e) {


                    String start_date_string = Client.getStart();
                    String end_date_string = Client.getEnd();

                    DateFormat df = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
                    try {
                        Date start_date = df.parse(start_date_string);
                        Date end_date = df.parse(end_date_string);
                        String newStartDateString = df.format(start_date);
                        String newEndDateString = df.format(end_date);

                        long diferenta = end_date.getTime() - start_date .getTime();
                        int returnedTime = (int) ((diferenta / (1000 * 60 * 60)) % 24);
                        //println2("\n Start Date " + start_date_string+ " End date " + end_date_string + " Diferenta " + Integer.toString(returnedTime));
                        if(returnedTime > -1)
                        {

                            println2("medication with id = " +  String.valueOf(finalI) + " has been taken !\n");
                            taken.setEnabled(false);
                            delete.add((Integer)finalI);
                            //System.out.println(delete);
                            messageDisplayer.selectAll();
                            messageDisplayer.replaceSelection("");
                            new Client(delete);
                            j++;
                        }
                    } catch (ParseException ei) {
                    ei.printStackTrace();
            } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                }
            });
            rowY += 80;
            logger.add(taken);
        }



        refresh = new JButton("Refresh");
        refresh.setBounds(310, 10, 100, 50);
        refresh.setBackground(Color.GREEN);
        refresh.addActionListener(this);


        refreshRate = new JSlider(JSlider.HORIZONTAL,
                0, 200, 100);
        refreshRate.addChangeListener(this);

        refreshRate.setSnapToTicks(true);
        refreshRate.setBounds(470, 10, 300, 50);
        refreshRate.setMajorTickSpacing(50);
        refreshRate.setMinorTickSpacing(10);
        refreshRate.setPaintTicks(true);
        refreshRate.setPaintLabels(true);


        label = new JLabel(" ");
        label.setBounds(200,0, 100,100);
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                while (true) {
                    Date date = getDate();
                    String dateString = simpleDateFormat.format(date);
                    label.setText(dateString);


                    try {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        Thread t = new Thread(runnable);
        t.start();

        wrapper.add(label);
        wrapper.add(refresh);
        wrapper.add(loggerScroller);
        wrapper.add(loggerScroller2);
        wrapper.setResizable(true);
        wrapper.getContentPane().setLayout(null);
        wrapper.setLocationRelativeTo(null);
        wrapper.setVisible(true);


    }

    public void println2(String str) {
        messageDisplayer.append(str + "\n");
        messageDisplayer.setCaretPosition(messageDisplayer.getText().length());
    }


    public static void main (String[] args)
    {
        new mainGUI(number);

    }

    @Override
    public void stateChanged(ChangeEvent e) {
        this.logger.setText("SALUT");
    }

    @Override
    // button listener
    public void actionPerformed(ActionEvent arg0)
    {
        if( arg0.getSource() == taken)
            this.messageDisplayer.setText("medication has been taken !");

        if( arg0.getSource() == refresh) {
            this.messageDisplayer.setText("Medication refreshed !");
            try {
                new Client(delete);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void prinln(String s) {
        logger.append(s + "\n");
        logger.setCaretPosition(logger.getText().length());
    }


    public static java.util.Date getDate() {
        java.util.Date date = new java.util.Date();
        return date;
    }

    public void prinln2(String s) {
        messageDisplayer.append(s + "\n");
        messageDisplayer.setCaretPosition(messageDisplayer.getText().length());
    }
}
