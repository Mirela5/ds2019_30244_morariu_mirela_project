package com.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.File;
import java.util.Random;
import java.util.Scanner;

public class Producer
{
    private static final String TASK_QUEUE_NAME = "task_queue";

    private final static String[] patients ={"13ca201e-f2ab-41f6-a9d6-7e2e45efd128","3d543240-8197-4070-bbae-d7c133bb15d2",
            "62b8c787-7b7a-4439-9ddd-a45d64922d8f","913a1844-736f-4cea-8b71-577ba7adfbec"};

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

            channel.exchangeDeclare(TASK_QUEUE_NAME,"topic",false);


            System.out.println(factory.getPort());


            // read from file
            System.out.println(factory.getPort());
            File file = new File("activity.txt");
            Random random = new Random();

            Scanner scanner = new Scanner(file);
            ObjectMapper mapper = new ObjectMapper();
            while (scanner.hasNextLine()) {
                String[] line = scanner.nextLine().split("\\s{2}+");
                int rand=random.nextInt(3);
                Data data = new Data(patients[rand],line[0], line[1], line[2]);
                String activityAsJson = mapper.writeValueAsString(data);
                channel.basicPublish("", TASK_QUEUE_NAME, null, activityAsJson.getBytes());

                System.out.println(" [x] Sent '" + activityAsJson + "'");
            }



        }
    }
}
