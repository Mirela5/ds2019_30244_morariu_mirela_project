package com.example.springdemo.consumer;

import com.example.springdemo.entities.Data;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class consumer {


    private final static String QUEUE_NAME = "task_queue";


    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            //  System.out.println(" [x] Received '" + message + "'");

            Rules(message);
        };

        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
        });


    }


    private static void Rules(String message)
    {
        List<String> allMatches = new ArrayList<String>();
        Matcher m = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2})")
                .matcher(message);

        while (m.find()) {
            allMatches.add(m.group());
            //  System.out.println("Found value: " + m.group(0));
        }

        String startDateString = allMatches.get(0);
        String endDateString = allMatches.get(1);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate;
        Date endDate;
        try {
            startDate = df.parse(startDateString);
            endDate = df.parse(endDateString);
            String newStartDateString = df.format(startDate);
            String newEndDateString = df.format(endDate);

            long diferenta = endDate.getTime() - startDate.getTime();
            Date dif = new Date(diferenta);
            int returnedTime = (int) ((diferenta / (1000 * 60 * 60)) % 24);

            if (message.contains("Sleeping")) {

                System.out.println("Start date : " + newStartDateString + "  End date: " + newEndDateString + " returnedTime " + returnedTime + "\n");
                if (returnedTime > 12)
                    System.out.println(" -> The sleep period is longer than 12 hours");
                else
                    System.out.println(" The sleep period is NO longer than 12 hours");
            }

            if (message.contains("Leaving")) {

                System.out.println("Start date : " + newStartDateString + "  End date: " + newEndDateString + " returnedTime " + returnedTime + "\n");
                if (returnedTime > 12)
                    System.out.println(" -> The leaving activity (outdoor) is longer than 12 hours");
                else
                    System.out.println("  The leaving activity (outdoor) is NO longer than 12 hours");
            }

            if (message.contains("Toileting") || message.contains("Showering")) {

                System.out.println("Start date : " + newStartDateString + "  End date: " + newEndDateString + " returnedTime " + returnedTime + "\n");
                if (returnedTime > 1)
                    System.out.println(" -> The period spent in bathroom is longer than 1 hour");
                else
                    System.out.println(" The period spent in bathroom is NO longer than 1 hour");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


}