package com.example.springdemo.entities;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name="patient")
public class Patient extends User{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name="medical_record")
    private String medicalRecord;

    @Column(name="doctor_id")
    private Integer doctorID;

    @ManyToOne @Fetch(FetchMode.JOIN)
    @JoinColumn(name="caregiver_id")
    private User caregiver;

    public Patient(Integer id, String username, Set<String> roles, String email, String name, String Data, String gender, String address, String medicalRecord, Integer doctorID, User caregiver) {
        super(id, username, roles, email, name, Data, gender, address);
        this.medicalRecord = medicalRecord;
        this.doctorID = doctorID;
        this.caregiver = caregiver;
    }

    public Patient(){}
    public Patient(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }



    public void setDoctorID(Integer doctorID) {
        this.doctorID = doctorID;
    }

    public void setCaregiver(User caregiver) {
        this.caregiver = caregiver;
    }

    public User getCaregiver() {
        return caregiver;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }


    public Patient(String medicalRecord, Integer doctorID) {
        this.medicalRecord = medicalRecord;
        this.doctorID = doctorID;
    }

    public Patient(Integer id, String username, Set<String> roles, String email, String name, String Data, String gender, String address, String medicalRecord, Integer doctorID) {
        super(id, username, roles, email, name, Data, gender, address);
        this.medicalRecord = medicalRecord;
        this.doctorID = doctorID;
    }

    public Patient(Integer id, String username, String password, String email, String name, String birthDate, String gender, String adress, String medicalRecord, Integer doctorID) {
        super(id, username, password, email, name, birthDate, gender, adress);
        this.medicalRecord = medicalRecord;
        this.doctorID = doctorID;
    }

    public Integer getDoctorID() {
        return doctorID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        if (!super.equals(o)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(getMedicalRecord(), patient.getMedicalRecord()) &&
                Objects.equals(getDoctorID(), patient.getDoctorID()) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getMedicalRecord(), getDoctorID());
    }
}
