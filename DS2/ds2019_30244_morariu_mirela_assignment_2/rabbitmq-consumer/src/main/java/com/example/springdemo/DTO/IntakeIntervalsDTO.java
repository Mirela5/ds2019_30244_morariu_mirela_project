package com.example.springdemo.DTO;

import java.io.Serializable;
import java.util.Date;

public class IntakeIntervalsDTO implements Serializable {

    private Integer id;
    private MedicationDTO medication;
    private String startDate;
    private String endDate;
    private String takes;

    public IntakeIntervalsDTO(Integer id, MedicationDTO medication, String startDate, String endDate, String takes) {
        this.id = id;
        this.medication = medication;
        this.startDate = startDate;
        this.endDate = endDate;
        this.takes = takes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MedicationDTO getMedication() {
        return medication;
    }

    public void setMedication(MedicationDTO medication) {
        this.medication = medication;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTakes() {
        return takes;
    }

    public void setTakes(String takes) {
        this.takes = takes;
    }
}
