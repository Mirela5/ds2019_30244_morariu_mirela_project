package com.example.springdemo.DTO;

import java.io.Serializable;

public class MedicationDTO implements Serializable {

    private Integer id;
    private String name;
    private String side_effects;
    private String dosage;

    public MedicationDTO(Integer id, String name, String side_effects, String dosage) {
        this.id = id;
        this.name = name;
        this.side_effects = side_effects;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return side_effects;
    }

    public void setSideEffects(String side_effects) {
        this.side_effects = side_effects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
